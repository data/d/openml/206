# OpenML dataset: triazines

https://www.openml.org/d/206

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

The problem is to learn a regression equation/rule/tree to predict the
 activity from the descriptive structural attributes.  The data and
 methodology is described in detail in: - King, Ross .D., Hurst,
 Jonathan. D., and Sternberg, Michael.J.E. A comparison of artificial
 intelligence methods for modelling QSARs Applied Artificial
 Intelligence, 1994 (in press).  - Hurst, Jonathan. D., King, Ross
 .D. and Sternberg, Michael.J.E. Quantitative Structure-Activity
 Relationships by neural networks and inductive logic programming:
 2. The inhibition of dihydrofolate reductase by triazines. Journal of
 Computer Aided Molecular Design, 1994 (in press).
 
 Original source: ?. 
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Characteristics: 186 cases; 61 continuous variables

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/206) of an [OpenML dataset](https://www.openml.org/d/206). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/206/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/206/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/206/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

